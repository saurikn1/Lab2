import java.util.HashMap
import java.util.LinkedHashMap

import java.lang.Math.pow

class Table(p: Probability, N: Int) {
    private var N = 0
    private var H = 0.0
    private val probability: Probability
    private val map = LinkedHashMap<Int, Double>()


    init {
        this.N = N
        probability = p.makeNewCharacters(N)
        probability.sort()
        probability.makeH()


        var n = 0

        var k = 1
        while (k < probability.size()) {
            k *= 2
            n++
        }
        map.put(n, 0.0)

        H = probability.h / N

        makeTable(n)
    }

    fun makeTable(n: Int) {
        var n = n
        var n_div_N = 0.0
        do {
            n--
            val lost = probability.size() - pow(2.0, n.toDouble()).toInt()
            val Pe = probability.countPe(lost)
            if (Pe != null) {
                map.put(n, Pe)
            }
            n_div_N = n.toDouble() / N.toDouble()

        } while (n_div_N > H)
    }


    override fun toString(): String {
        val res = StringBuilder()

        System.out.println(probability)

        res.append("N = " + N + "\n")
        val it = map.keys.iterator()

        while (it.hasNext()) {
            val n = it.next()
            val n_div_N = n.toDouble() / N.toDouble()
            if (it.hasNext())
                res.append("n = " + n + "| R = " + String.format("%.5f", n_div_N) + " > H = " + String.format("%.5f", H) + "| Pe = " + String.format("%.5f", map[n]) + "\n")
            else
                res.append("n = " + n + "| R = " + String.format("%.5f", n_div_N) + " < H = " + String.format("%.5f", H) + "| Pe = " + String.format("%.5f", map[n]) + "\n")
        }

        return res.toString()
    }


}
