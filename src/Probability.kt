import java.util.ArrayList
import java.util.Comparator
import java.util.Random


import java.lang.Math.*
import javax.swing.text.Element


class Probability {


    private val characters_probability = ArrayList<Elem>()
    var h = 0.0
        private set


    internal class Elem(s: String, p: Double?) {
        var characters = ""
        var probability = 0.0

        init {
            characters = s
            probability = p!!
        }

        override fun equals(obj: Any?): Boolean {
            if (obj is Elem) {
                val tmp = obj as Elem?
                return if (this.characters == tmp!!.characters && this.probability == tmp.probability) {
                    true
                } else {
                    false
                }
            }

            if (obj is String) {
                val tmp = obj as String?
                return if (this.characters == tmp) {
                    true
                } else {
                    false
                }
            }

            if (obj is Double) {
                val tmp = obj as Double?
                return if (this.probability == tmp) {
                    true
                } else {
                    false
                }
            }

            return false


        }

        companion object {

            val COMPARE_BY_PROBABILITY: Comparator<Elem> = Comparator { t1, t2 ->
                //Double.compare()
                java.lang.Double.compare(t1.probability, t2.probability)
            }
        }
    }

    constructor() {}

    constructor(str: String) {

        for (i in 0 until str.length) {
            add(str.substring(i, i + 1))
        }

        div(str.length)
    }

    fun makeString(length: Int): String {

        val random = Random(System.currentTimeMillis())
        val res = StringBuilder()

        for (i in 0 until length) {
            val tmp = random.nextInt(1000)
            var d = (tmp % 1000).toDouble()
            d /= 1000.0
            res.append(getChars(d))
        }

        return res.toString()

    }

    fun makeNewCharacters(n: Int): Probability {
        val res = Probability()

        recursia(n, StringBuffer(), res)

        return res
    }

    fun recursia(n: Int, stringBuffer: StringBuffer, res: Probability) {

        if (n - 1 == 0) {
            for (i in characters_probability.indices) {
                stringBuffer.append(characters_probability[i].characters)


                var probability = 1.0

                for (j in 0 until stringBuffer.length) {
                    val tmp = stringBuffer[j].toString()
                    val it = characters_probability.listIterator()
                    while (it.hasNext()) {
                        val tmpElem = it.next()
                        if (tmpElem.characters == tmp) {
                            probability *= tmpElem.probability
                            break
                        }
                    }
                }

                res.addBoth(stringBuffer.toString(), probability)

                stringBuffer.deleteCharAt(stringBuffer.length - 1)
            }
        } else {
            for (i in characters_probability.indices) {
                stringBuffer.append(characters_probability[i].characters)
                recursia(n - 1, stringBuffer, res)
                stringBuffer.deleteCharAt(stringBuffer.length - 1)
            }
        }
    }

    fun addBoth(s: String, p: Double) {
        if (!characters_probability.contains(s)) {
            characters_probability.add(Elem(s, p))
        }
    }


    fun makeH() {
        for (i in characters_probability.indices) {
            h -= characters_probability[i].probability * (log10(characters_probability[i].probability) / log10(2.0))
        }
    }


    fun add(str: String) {

        val it = characters_probability.listIterator()
        while (it.hasNext()) {
            val tmp = it.next()
            if (tmp.characters == str) {
                tmp.probability++
                return
            }
        }

        characters_probability.add(Elem(str, 1.0))

    }

    operator fun div(value: Int) {

        val it = characters_probability.listIterator()
        while (it.hasNext()) {
            val tmp = it.next()
            tmp.probability /= value.toDouble()

        }
    }


    fun getChars(d: Double): String {
        var tmp = 0.0
        for (i in characters_probability.indices) {
            val tmpElem = characters_probability[i]
            if (d >= tmp && d < tmp + tmpElem.probability) {
                return tmpElem.characters
            } else {
                tmp += tmpElem.probability
            }
        }
        return ""
    }

    fun size(): Int {
        return characters_probability.size
    }

    fun sort() {
        characters_probability.sortWith(Elem.COMPARE_BY_PROBABILITY)
    }

    fun countPe(lost: Int): Double? {
        var res: Double? = 0.0
        for (i in 0 until lost) {
            res = res?.plus(characters_probability[i].probability)
        }

        return res
    }

    override fun toString(): String {
        val res = StringBuilder()

        for (i in characters_probability.indices) {
            res.append(characters_probability[i].characters + "=" + String.format("%.5f", characters_probability[i].probability) + "; ")
        }

        return res.toString()
    }


}